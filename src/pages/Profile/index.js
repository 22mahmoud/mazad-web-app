import React, {Component} from 'react';
import {
  TextField,
  Button,
  Card,
  CardHeader,
  Avatar,
  Divider,
} from '@material-ui/core/';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import NavBar from '../../components/NavBar';

const styles = theme => ({
  card: {
    display: 'flex',
    margin: 30,
    padding: 20,
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
  },
  avatar: {
    margin: 10,
    width: '200px',
    height: '200px',
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    '&:hover': {
      boxShadow:
        '0px 14px 28px rgba(0,0,0,0.25), 0px 10px 10px rgba(0,0,0,0.22)',
    },
  },
  form: {
    marginLeft: '50px',
    width: '80%',
  },
  sec: {
    boxShadow: '3 3px 6px rgba(0,0,0,0.06), 3 3px 6px rgba(0,0,0,0.13)',
    marginBottom: '30px',
  },
  listAddresses: {
    margin: 20,
    padding: 20,
  },
});

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 'Info',

      editEnabled: false,
      FirstName: '',
      LastName: '',
      Pass: '',
      Email: '',

      type: 'All',

      addAddress: false,
      list: [],
      addresses: [],
    };
  }

  /*
   *    * renderInfo:Presentetional Function Component
   *       * return Custom Text Filed
   *          * @param(Object, index)
   *             * Object: {labe: the titl of the text field , name: the state param }
   *                * it stores the value of the text field when `editEnabled`
   *                   * in new+`name`
   *                      * it reads the value of `name`
   *                         **/
  renderInfo({label, name}, index) {
    let {state, props} = this;
    let {editEnabled} = state;
    const {classes} = props;
    let val = editEnabled ? 'new' + name : name;
    return (
      <TextField
        key={index}
        className={classes.tf}
        label={label}
        disabled={!editEnabled}
        style={{margin: 8}}
        margin="normal"
        variant="outlined"
        InputLabelProps={{shrink: true}}
        value={this.state[val]}
        onChange={e => {
          this.setState({[val]: e.target.value});
        }}
      />
    );
  }

  onEdit() {
    const {editEnabled, FirstName, LastName, Pass, Email} = this.state;
    this.setState({
      editEnabled: !editEnabled,
      newFirstName: FirstName,
      newLastName: LastName,
      newPass: Pass,
      newEmail: Email,
    });
  }

  submitInfo() {
    const {
      editEnabled,
      newFirstName,
      newLastName,
      newPass,
      newEmail,
    } = this.state;
    this.setState({
      editEnabled: !editEnabled,
      FirstName: newFirstName,
      LastName: newLastName,
      Pass: newPass,
      Email: newEmail,
    });
  }

  renderAddress({label, name}, index) {
    let {state, props} = this;
    let {addAddress} = state;
    const {classes} = props;
    let val = addAddress ? name : name;
    return (
      <TextField
        key={index}
        className={classes.tf}
        label={label}
        disabled={!addAddress}
        style={{margin: 8}}
        margin="normal"
        variant="outlined"
        InputLabelProps={{shrink: true}}
        value={this.state[val]}
        onChange={e => {
          this.setState({[val]: e.target.value});
        }}
      />
    );
  }

  onNew() {
    const {addAddress} = this.state;
    this.setState({
      addAddress: !addAddress,
      Address: '',
      City: '',
      Zip: '',
      Instruction: '',
    });
  }

  submitAddress() {
    const {
      list,
      addresses,
      addAddress,
      Address,
      City,
      Zip,
      Instruction,
    } = this.state;
    if (Address != '' && City != '' && Zip != '') {
      this.setState({
        addAddress: !addAddress,
        list: [Address, City, Zip, Instruction],
        add: addresses.push(list),
      });
    }
  }

  onSwitchTab(value) {
    this.setState({tab: value});
  }

  onSwitchDetails(v) {
    this.setState({type: v});
  }

  render() {
    const {classes} = this.props;
    let {tab, editEnabled, type, addAddress} = this.state;

    return (
      <div>
				<NavBar />
        <Card className={classes.card}>
          <div style={{flexDirection: 'column', display: 'flex'}}>
            <Avatar
              alt="R"
              src={require('../../it.jpg')}
              className={classes.avatar}
            />
            {['Info', 'Payment', 'Address'].map((value, index) => {
              return (
                <Button key={index} onClick={() => this.onSwitchTab(value)}>
                  {value}
                </Button>
              );
            })}
          </div>
          <form className={classes.form}>
            {tab == 'Info' && (
              <div className={classes.sec}>
                <div
                  style={{
                    display: 'flex',
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'space-between',
                  }}>
                  <CardHeader title="Info" />
                </div>
                <div style={{display: 'flex', flexDirection: 'column'}}>
                  <div style={{display: 'flex', flexDirection: 'column'}}>
                    {[
                      {label: 'First Name', name: 'FirstName'},
                      {label: 'Last Name', name: 'LastName'},
                      {label: 'Paswword', name: 'Pass'},
                      {label: 'E-mail', name: 'Email'},
                    ].map((val, index) => {
                      return this.renderInfo(val, index);
                    })}
                  </div>
                  <div
                    style={{
                      margin: 10,
                      dispaly: 'flex',
                      flexDirection: 'column',
                    }}>
                    <Button onClick={() => this.onEdit()}>
                      {editEnabled ? 'Undo' : 'Edit'}
                    </Button>

                    {editEnabled && (
                      <Button onClick={() => this.submitInfo()}>Apply</Button>
                    )}
                  </div>
                </div>
              </div>
            )}

            {tab == 'Payment' && (
              <div className={classes.sec}>
                <div
                  style={{
                    display: 'flex',
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'space-between',
                  }}>
                  <CardHeader title="Payment" />
                </div>
                <div style={{display: 'flex', flexDirection: 'column'}}>
                  <div style={{display: 'flex'}}>
                    {['All', 'Received', 'Spent'].map((v, index) => {
                      return (
                        <Button
                          style={{width: '33%'}}
                          key={index}
                          onClick={() => this.onSwitchDetails(v)}>
                          {v}
                        </Button>
                      );
                    })}
                  </div>

                  {type == 'All' && (
                    <div>
                      <p>Display All</p>
                    </div>
                  )}

                  {type == 'Received' && (
                    <div>
                      <p>Display Received</p>
                    </div>
                  )}

                  {type == 'Spent' && (
                    <div>
                      <p>Display Spent</p>
                    </div>
                  )}
                </div>
              </div>
            )}

            {tab == 'Address' && (
              <div className={classes.sec}>
                <div
                  style={{
                    display: 'flex',
                    flex: 1,
                    flexDirection: 'row',
                    alignItems: 'space-between',
                  }}>
                  <CardHeader title="Addresses" />
                </div>
                <div style={{display: 'flex', flexDirection: 'column'}}>
                  <div
                    style={{
                      margin: 10,
                      dispaly: 'flex',
                      flexDirection: 'column',
                    }}>
                    <Button onClick={() => this.onNew()}>
                      {addAddress ? 'Cancel' : 'New Address'}
                    </Button>

                    {addAddress && (
                      <Button onClick={() => this.submitAddress()}>Add</Button>
                    )}
                  </div>
                  {addAddress && (
                    <div style={{display: 'flex', flexDirection: 'column'}}>
                      {[
                        {label: 'Address', name: 'Address'},
                        {label: 'City', name: 'City'},
                        {label: 'Zip', name: 'Zip'},
                        {
                          label: 'Instruction to delivery (optional)',
                          name: 'Instruction',
                        },
                      ].map((val, index) => {
                        return this.renderAddress(val, index);
                      })}
                    </div>
                  )}

                  <div className={classes.listAddresses}>
                    {this.state.addresses.map(item => (
                      <div className={classes.items}>
                        <Divider />
                        <Divider />
                        <p key={item[0]}>Address: {item[0]}</p>
                        <p key={item[1]}>City: {item[1]}</p>
                        <p key={item[2]}>Zip Code: {item[2]}</p>
                        <p key={item[3]}>Instruction to delivery: {item[3]}</p>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            )}
          </form>
        </Card>
      </div>
    );
  }
}

Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Profile);
