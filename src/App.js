import React, {Component} from 'react';
import {ThemeProvider} from '@material-ui/styles';
import {Switch, BrowserRouter, Route} from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from 'react-apollo';
import CssBaseline from '@material-ui/core/CssBaseline';

import theme from './theme';
import PrivateRoute from './components/PrivateRoute';
import Home from './pages/Home';
import Register from './pages/Register';
import Auctions from './pages/Auctions';
import Auction from './pages/Auction';
import Profile from './pages/Profile';
import CreateAuction from './pages/CreateAuction';

const client = new ApolloClient({
  uri: 'https://mazzad.herokuapp.com/v1alpha1/graphql',
  request: async operation => {
    const token = await localStorage.getItem('token');
    operation.setContext({
      headers: {
        authorization: token ? `Bearer ${token}` : '',
      },
    });
  },
});

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login" component={Register} />
              <Route exact path="/auctions" component={Auctions} />
              <PrivateRoute
                exact
                path="/auctions/new"
                component={CreateAuction}
              />
              <Route exact path="/auctions/:id" component={Auction} />
              <PrivateRoute exact path="/user/:id" component={Profile} />
            </Switch>
          </BrowserRouter>
        </ThemeProvider>
      </ApolloProvider>
    );
  }
}

export default App;
