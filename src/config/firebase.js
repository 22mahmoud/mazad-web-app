import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/functions';

const config = {
  apiKey: 'AIzaSyDTvW6R9Dbrt8Xg_GyXmR-pkYftBv9Xeok',
  authDomain: 'mazzad-a47a9.firebaseapp.com',
  databaseURL: 'https://mazzad-a47a9.firebaseio.com',
  projectId: 'mazzad-a47a9',
  storageBucket: 'mazzad-a47a9.appspot.com',
  messagingSenderId: '105970090406',
};

export default firebase.initializeApp(config);
