import React, { Component } from 'react';
import { Divider } from '@material-ui/core/';
import Pcont from './pcont.js';
import NavBar from './nav.js';

class Profile extends Component {
    render() {
        return(
            <div style={{width:'100%'}}>
                <NavBar/>
                <Pcont/>
            </div>
        );
    }
}

export default Profile;