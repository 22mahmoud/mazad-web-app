import React, {Component} from 'react';
import {Button, Card, Snackbar, SnackbarContent} from '@material-ui/core/';
import firebase from 'firebase/app';
import ErrorIcon from '@material-ui/icons/Error';
import {withStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import {Mutation, ApolloConsumer} from 'react-apollo';
import gql from 'graphql-tag';

import app from '../../config/firebase';

const styles = theme => ({
  root: {
    display: 'flex',
    flex: 1,
    minHeight: '100vh',
  },
  right: {
    width: '30%',
    backgroundColor: 'orange',
    display: 'flex',
    flex: 3,
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
  },
  form: {
    flex: 9,
    float: 'none',
    margin: '0 auto',
  },
  card: {
    display: 'flex',
    padding: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 20,
    error: {
      backgroundColor: theme.palette.error.dark,
    },
    icon: {
      fontSize: 20,
      opacity: 0.9,
      marginRight: theme.spacing(1),
    },
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    '&:hover': {
      boxShadow:
        '0px 14px 28px rgba(0,0,0,0.25), 0px 10px 10px rgba(0,0,0,0.22)',
    },
  },
});

const CREATE_PROFILE_MUTATION = gql`
  mutation CreateProfileMutation(
    $email: String
    $firstName: String
    $lastName: String
    $phoneNumber: String
    $id: String
  ) {
    insert_profiles(
      objects: {
        email: $email
        first_name: $firstName
        id: $id
        last_name: $lastName
        phone_number: $phoneNumber
      }
    ) {
      returning {
        id
        first_name
        last_name
      }
    }
  }
`;

const PROFILE_QUERY = gql`
  query ProfileQuery($email: String) {
    profiles(where: {email: {_eq: $email}}) {
      email
    }
  }
`;

class Register extends Component {
  state = {
    error: '',
  };

  googleSignin = async () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    const {user} = await app.auth().signInWithPopup(provider);
    const {uid, email, phoneNumber, displayName} = user;
    const [firstName, lastName] = displayName.split(' ');
    const token = await user.getIdToken();
    return {
      uid,
      email,
      phoneNumber,
      firstName,
      lastName,
      token,
    };
  };

  render() {
    const {classes} = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.right} />
        <div className={classes.form}>
          <Card className={classes.card}>
            <form
              style={{
                display: 'flex',
                flexDirection: 'column',
                float: 'none',
                margin: '0 auto',
              }}
              noValidate
              autoComplete="off">
              <ApolloConsumer>
                {client => (
                  <Mutation mutation={CREATE_PROFILE_MUTATION}>
                    {mutate => (
                      <>
                        <Button
                          variant="contained"
                          onClick={async () => {
                            try {
                              const {
                                email,
                                uid,
                                lastName,
                                firstName,
                                token,
                                phoneNumber,
                              } = await this.googleSignin();

                              const isAlreadyRegisterd = await client.query({
                                query: PROFILE_QUERY,
                                variables: {
                                  email,
                                },
                              });

                              if (isAlreadyRegisterd.data.profiles.length) {
                                /* do nonthing */
                              } else {
                                await mutate({
                                  variables: {
                                    email,
                                    firstName,
                                    lastName,
                                    phoneNumber: phoneNumber || '',
                                    id: uid,
                                  },
                                });
                              }

                              localStorage.setItem('token', token);
                              this.props.history.replace('/');
                            } catch (error) {
                              this.setState({
                                error: error.message,
                              });
                            }
                          }}>
                          Login
                        </Button>
                        <Snackbar
                          anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                          }}
                          open={this.state.error}
                          autoHideDuration={6000}
                          onClose={() => {
                            this.setState({
                              error: '',
                            });
                          }}>
                          <SnackbarContent
                            className={classes.error}
                            aria-describedby="client-snackbar"
                            message={
                              <span
                                id="client-snackbar"
                                className={classes.message}>
                                <ErrorIcon className={classes.icon} />
                                {this.state.error}
                              </span>
                            }
                            action={[
                              <IconButton
                                key="close"
                                aria-label="Close"
                                color="inherit"
                                onClick={() => {
                                  this.setState({
                                    isError: false,
                                  });
                                }}>
                                <CloseIcon
                                  style={{
                                    fontSize: 20,
                                  }}
                                  className={classes.icon}
                                />
                              </IconButton>,
                            ]}
                          />
                        </Snackbar>
                      </>
                    )}
                  </Mutation>
                )}
              </ApolloConsumer>
            </form>
          </Card>
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Register);
