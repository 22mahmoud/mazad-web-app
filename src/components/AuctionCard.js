import React from 'react';
import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Button,
  Typography,
} from '@material-ui/core';
import {withRouter} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const styles = {
  card: {
    Width: '100%',
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
    '&:hover': {
      boxShadow:
        '0px 14px 28px rgba(0,0,0,0.25), 0px 10px 10px rgba(0,0,0,0.22)',
    },
  },
  all: {
    display: 'flex',
  },
  media: {
    height: 140,
    width: 140,
  },
};

function AuctionCard(props) {
  const {classes} = props;
  return (
    <Card
      onClick={() => props.history.push('/auctions/4')}
      className={classes.card}>
      <CardActionArea>
        <div style={{display: 'flex'}}>
          <CardMedia
            className={classes.media}
            image={require('../it.jpg')}
            height="100%"
            width="100%"
            title="ITEM"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              Item
            </Typography>
            <Typography component="p">Info about the item</Typography>
          </CardContent>
        </div>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary">
          More Details
        </Button>
      </CardActions>
    </Card>
  );
}

AuctionCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(AuctionCard));
