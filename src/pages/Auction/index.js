import React, {Component} from 'react';
import {TextField, Typography, Button, Grid, Card} from '@material-ui/core/';
import {withStyles} from '@material-ui/core/styles';

import NavBar from '../../components/NavBar';

const styles = theme => ({
  showData: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 20,
    marginRight: 20,
    marginLeft: 20,
    padding: 20,
    boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
  },
});

class Auction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: 'Bidding',
      Condition: 'Used',
      Region: 'England',
      Category: 'Antique, Phones',
      Number: '564646',
      Description: 'Write Here',
      price: 25.27,
      currentBid: '00.00',
      bid: 0,
    };
  }

  onSwitch(value) {
    this.setState({show: value});
  }

  renderAbout({name, text}, index) {
    return (
      <Typography style={{margin: '15px'}}>
        {' '}
        {name}: {text}{' '}
      </Typography>
    );
  }

  renderBidding({name}, index) {
    let val = name;
    if (index == 1) {
      return (
        <div style={{margin: 10}}>
          <Typography>
            {' '}
            <b> {name} </b>{' '}
          </Typography>
        </div>
      );
    } else if (index == 3) {
      return (
        <div style={{margin: 10}}>
          <TextField
            key={index}
            placeholder={name}
            value={this.state[val]}
            inputProps={{
              style: {textAlign: 'center', width: '100px', height: '0px'},
            }}
            margin="normal"
            variant="outlined"
            onChange={e => {
              this.setState({[val]: e.target.value});
            }}
          />
        </div>
      );
    } else {
      return (
        <div style={{margin: 10}}>
          <Typography> {name} </Typography>
        </div>
      );
    }
  }

  renderIncBidding({name, onClick}, index) {
    let {props} = this;
    return (
      <div>
        <Button
          variant="outlined"
          color="primary"
          style={{margin: '20px'}}
          onClick={onClick}>
          {' '}
          {name}{' '}
        </Button>
      </div>
    );
  }

  confirm() {
    const {currentBid} = this.state;
    this.setState({
      currentBid: currentBid,
      price: parseFloat(currentBid),
    });
  }

  render() {
    const {classes} = this.props;
    let {
      show,
      Condition,
      Region,
      Category,
      Description,
      price,
      currentBid,
    } = this.state;
    return (
      <>
        <NavBar />
        <div style={{display: 'flex', flexDirection: 'column'}}>
          <Grid
            container
            alignItems="center"
            justify="center"
            style={{marginTop: 20, marginBottom: 20, display: 'flex'}}>
            {['About the item', 'Bidding', 'Shipping & Return'].map(
              (value, index) => {
                return (
                  <Button
                    color="primary"
                    style={{width: '17%'}}
                    key={index}
                    onClick={() => this.onSwitch(value)}>
                    {value}
                  </Button>
                );
              },
            )}
          </Grid>

          {show === 'About the item' && (
            <div>
              <Card className={classes.showData}>
                {[
                  {name: 'Condition', text: Condition},
                  {name: 'Region of Manufacture', text: Region},
                  {name: 'Category', text: Category},
                  {name: 'Description', text: Description},
                ].map((val, index) => {
                  return this.renderAbout(val, index);
                })}
              </Card>
            </div>
          )}

          {show === 'Bidding' && (
            <div>
              <Card
                className={classes.showData}
                style={{textAlign: 'center', alignItems: 'center'}}>
                <div>
                  {[
                    {name: 'Top Bid'},
                    {name: 'EGP ' + price},
                    {name: 'Place your bid: '},
                    {name: currentBid},
                    {name: 'Or choose'},
                  ].map((val, index) => {
                    return this.renderBidding(val, index);
                  })}
                </div>
                <div style={{display: 'flex', flexDirection: 'row'}}>
                  {[
                    {name: '+5 EGP', onClick: price + 5},
                    {name: '+10 EGP', onClick: price + 10},
                    {name: '+15 EGP', onClick: price + 15},
                  ].map((val, index) => {
                    return this.renderIncBidding(val, index);
                  })}
                </div>
                <p>{parseFloat(currentBid)}</p>
                <Button
                  variant="outlined"
                  color="primary"
                  style={{margin: '20px', width: '15%'}}
                  onClick={() => this.confirm()}>
                  Confirm
                </Button>
              </Card>
            </div>
          )}

          {show === 'Shipping & Return' && <div />}
        </div>
      </>
    );
  }
}

export default withStyles(styles)(Auction);
