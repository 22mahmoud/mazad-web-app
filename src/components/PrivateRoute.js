import React from 'react';
import {withRouter, Route, Redirect} from 'react-router-dom';
import {Query} from 'react-apollo';
import gql from 'graphql-tag';

const PROFILE_QUERY = gql`
  query ProfileQuery($email: String) {
    profiles {
      id
    }
  }
`;

function PrivateRoute({history, component: Component, ...rest}) {
  return (
    <Route
      {...rest}
      render={props => (
        <Query query={PROFILE_QUERY}>
          {({loading, error, data}) => {
            if (loading) return null;
            if (error) {
              return (
                <Redirect
                  to={{pathname: '/login', state: {from: props.location}}}
                />
              );
            }

            const {
              profiles: [me],
            } = data;

            return me.id ? (
              <Component {...props} />
            ) : (
              <Redirect
                to={{pathname: '/login', state: {from: props.location}}}
              />
            );
          }}
        </Query>
      )}
    />
  );
}

export default withRouter(PrivateRoute);
