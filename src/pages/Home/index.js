import React from 'react';

import NavBar from '../../components/NavBar';
import AuctionCard from '../../components/AuctionCard';

function Home() {
  return (
    <div style={{width: '100%'}}>
      <NavBar icons={{mail: true}} />
      <div
        style={{
          margin: 20,
          flexWrap: 'wrap',
          display: 'flex',
          flexDirection: 'row',
        }}>
        {Array.from({length: 20}).map((_, i) => (
          <div key={i} style={{margin: 10}}>
            <AuctionCard />
          </div>
        ))}
      </div>
    </div>
  );
}

export default Home;
